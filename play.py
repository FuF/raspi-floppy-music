import RPi.GPIO as g
import time
import sys
import os
import time
import notes
import json

step = 15
direction = 16

noteconst = 3.14


if not os.path.isfile(sys.argv[1]):
   print('File ' + sys.argv[1] + ' does not exists')
   sys.exit()

try:
   f = open(sys.argv[1])
   data = json.load(f)
except ValueError:
   print('Bad json data ' + e)
   sys.exit()

g.setmode(g.BOARD)
g.setup(step, g.OUT)
g.setup(direction, g.OUT)
g.output(direction, True)

tempo = data['tempo']
song = data['song']

x = 0

while x < 200:
   x = x + 1
   g.output(step, True)
   g.output(step, False)
   time.sleep(0.01)

x = 0
y = False

g.output(direction, False)
for note, octave, duration, legato in song:
   if not legato:
       time.sleep(noteconst/128)
   t = time.time()*1000 + (tempo / duration)

   print(note +  octave.__str__() + " 1/" + duration.__str__())

   while t > (time.time() * 1000):
      try:
         if x > 78:
            x = 0
            y = not y
            g.output(direction, y)
         if note == 'Zz':
            time.sleep(0.01)
            continue
         
         g.output(step, True)
         g.output(step, False)
         x += 1
         time.sleep(noteconst/notes.notes[note][octave])

      except (KeyboardInterrupt, SystemExit()):
         g.cleanup()

g.cleanup()
